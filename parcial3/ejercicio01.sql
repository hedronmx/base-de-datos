/*Mostrar una lista de clientes que esten en la misma ciudad*/
SELECT A.customerNumber AS a1,
       B.customerNumber AS a2,
       A.city
FROM customers A,
     customers B
WHERE A.customerNumber <> B.customerNumber
  AND A.city = B.city
ORDER BY A.city;

/*Mostrar el vendedor de productos donde el mayor pedido sea de 6300 o mas*/
SELECT productVendor
FROM products
INNER JOIN orderdetails USING(productCode)
GROUP BY productVendor
HAVING SUM(priceEach * quantityOrdered) >= 6300